# Pressing a **button** in **top-right** corner in mobile mode activates **menu**. Menu turns off by **clicking** the **button again**. #
# It is as *responsive* as i could achieve using only pure **HTML5/CSS3**. #

## Set to fit into **320** pixels, iPhone 5 size. Maximum width is set to **1500px**, just as wide as markup was. ##